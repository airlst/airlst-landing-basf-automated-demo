module.exports = {
	plugins: {
		"autoprefixer": [
    ">= 1%",
    "last 1 major version",
    "dead",
    "Chrome >= 45",
    "Firefox >= 38",
    "Edge >= 12",
    "Explorer >= 10",
    "iOS >= 9",
    "Safari >= 9",
    "Android >= 4.4",
    "Opera >= 30"
  	]
	}
};