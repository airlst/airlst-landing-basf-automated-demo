import template from './component.html';
import BookingsRecommendationsController from './controller';

export default {
  template: template,
  controller: BookingsRecommendationsController,
  controllerAs: 'vm',
  bindings: {
    recommendationsData: '<',
    eventInformation: '<',
    onDataChange: '&'
  }
};
